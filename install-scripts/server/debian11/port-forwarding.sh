#!/usr/bin/sh
source_ip_external="185.110.247.52"
source_ip_internal="192.168.122.1"
source_port_start=10000
source_port_end=11000
destination_ip="192.168.122.210"
destination_port_start=10000
destination_port_end=11000

iptables -I FORWARD -d "$destination_ip" -j ACCEPT
iptables -I FORWARD -s "$destination_ip" -j ACCEPT
iptables -t nat -I PREROUTING -p tcp -d "$source_ip_external" --dport "$source_port_start:$source_port_end" -j DNAT --to-destination "$destination_ip:$destination_port_start-$destination_port_end"
iptables -t nat -I POSTROUTING -p tcp -d "$destination_ip" --dport "$destination_port_start:$destination_port_end" -j SNAT --to-source "$source_ip_internal"
iptables -t nat -I PREROUTING -p udp -d "$source_ip_external" --dport "$source_port_start:$source_port_end" -j DNAT --to-destination "$destination_ip:$destination_port_start-$destination_port_end"
iptables -t nat -I POSTROUTING -p udp -d "$destination_ip" --dport "$destination_port_start:$destination_port_end" -j SNAT --to-source "$source_ip_internal"
