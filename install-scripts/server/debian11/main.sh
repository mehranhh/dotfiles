#!/usr/bin/sh
echo "deb http://deb.debian.org/debian bullseye main
deb-src http://deb.debian.org/debian bullseye main
deb http://security.debian.org/debian-security bullseye-security main
deb-src http://security.debian.org/debian-security bullseye-security main
deb http://deb.debian.org/debian bullseye-updates main
deb-src http://deb.debian.org/debian bullseye-updates main
deb http://deb.debian.org/debian bullseye-backports main
deb-src http://deb.debian.org/debian bullseye-backports main" > /etc/apt/sources.list

echo "Acquire {
  HTTP::proxy \"http://127.0.0.1:10809\";
  HTTPS::proxy \"http://127.0.0.1:10809\";
}" >> /etc/apt/apt.conf.d/proxy.conf

apt update && apt upgrade -y && apt install -y curl fish tmux vim rsync docker docker-compose acl certbot git nload htop ncdu wireguard wireguard-tools resolvconf

# insatll docker
apt-get update && apt-get install ca-certificates curl gnupg && install -m 0755 -d /etc/apt/keyrings && curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && chmod a+r /etc/apt/keyrings/docker.gpg && echo "deb [arch=\"$(dpkg --print-architecture)\" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian" "$(. /etc/os-release && echo "$VERSION_CODENAME")" "stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null && apt-get update && apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

groupadd shared
mkdir /shared
setfacl -Rdm g:shared:rwx /shared
setfacl -Rm g:shared:rwx /shared

# user creation
useradd -mG sudo,docker,shared -s "$(which fish)" mhn
passwd root
passwd mhn
mkdir /home/mhn/.ssh
nano /home/mhn/.ssh/authorized_keys
chown -R mhn:mhn /home/mhn/.ssh

# docker proxy in case you need it
mkdir -p /etc/systemd/system/docker.service.d
echo "[Service]
Environment=\"HTTP_PROXY=http://localhost:10809\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
#[Service]
#Environment="HTTP_PROXY=http://localhost:10809"
systemctl daemon-reload
systemctl restart docker

# xray config
bash -c "$(curl -L https://github.com/XTLS/Xray-install/raw/main/install-release.sh)" @ install --beta -u root
nano /usr/local/etc/xray/config.json
systemctl enable --now xray

# get a cert
sudo certbot certonly --standalone --agree-tos --register-unsafely-without-email --preferred-challenges http -d s1.domain.com
sudo certbot -d '*.domain.com' -d domain.com --manual --agree-tos --register-unsafely-without-email --preferred-challenges dns certonly

# sshd: PermitRootLogin no && PasswordAuthentication no
nano /etc/ssh/sshd_config
systemctl restart sshd.service

reboot
