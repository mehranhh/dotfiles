#!/bin/bash
if [ "$EUID" -ne 0 ]
    then echo "Please run as root user"
    exit
fi
ln -sf /etc/runit/sv/NetworkManager /run/runit/service/NetworkManager
nmtui
ln -sf /etc/runit/sv/bluetoothd /run/runit/service/bluetoothd 
pacman -S artix-archlinux-support
echo "[lib32]" >> /etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf
echo "" >> /etc/pacman.conf
echo "[extra]" >> /etc/papacman.conf
echo "# Arch" >> /etc/papacman.conf
echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/papacman.conf
echo "" >> /etc/pacman.conf
echo "[community]" >> /etc/papacman.conf
echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/papacman.conf
echo "" >> /etc/pacman.conf
echo "[multilib]" >> /etc/pacpacman.conf
echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/papacman.conf
pacman-key --populate archlinux
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist-artix
pacman -Syu mesa lib32-mesa vulkan-intel lib32-vulkan-intel nvidia nvidia-utils nvidia-prime nvidia-dkms nvidia-settings lib32-nvidia-utils\
    xorg lightdm lightdm-runit lightdm-slick-greeter lightdm-webkit2-greeter lightdm-webkit-theme-litarvan pacman-contrib
ln -sf /etc/runit/sv/lightdm /run/runit/service/lightdm 
