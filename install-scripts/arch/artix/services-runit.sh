#!/bin/bash
if [ "$EUID" -ne 0 ]
    then echo "Please run as root user"
    exit
fi
pacman -S lightdm-runit docker-runit bluez-runit networkmanager-runit libvirtd-runit tor-runit ufw-runit openssh-runit
#service: bluetoothd tor NetworkManager lightdm libvirtd virtlogd docker ufw openssh
