#!/bin/bash
if [ "$EUID" -eq 0 ]
    then echo "Please run as a none root user"
    exit
fi
sudo pacman -S ttf-hack ttf-font-awesome ttf-joypixels ttf-roboto inter-font ttf-fira-code ttf-hack-nerd ttf-sourcecodepro-nerd ttf-firacode-nerd ttf-iosevka-nerd \
    noto-fonts-cjk noto-fonts-emoji noto-fonts
paru -S borna-fonts iranian-fonts ir-standard-fonts vazirmatn-fonts vazir-code-fonts
