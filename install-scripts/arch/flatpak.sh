#!/bin/bash
if [ "$EUID" -eq 0 ]
  then echo "Please run as a none root user"
  exit
fi
sudo pacman -S flatpak
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --if-not-exists kdeapps https://distribute.kde.org/kdeapps.flatpakrepo

# global
flatpak override --user --filesystem=~/.themes:ro
flatpak override --user --filesystem=xdg-config/gtk-4.0:ro
flatpak override --user --filesystem=xdg-config/gtk-3.0:ro
flatpak override --user --filesystem=xdg-config/gtk-2.0:ro
flatpak override --user --filesystem=xdg-config/gtkrc-2.0:ro
flatpak override --user --filesystem=xdg-config/gtkrc:ro
flatpak override --user --filesystem=xdg-config/qt5ct:ro
flatpak override --user --filesystem=xdg-config/Kvantum:ro
flatpak override --user --filesystem=~/.themes

flatpak override --user --env=GTK_THEME=Adwaita-dark
flatpak override --user --env=QT_STYLE_OVERRIDE=kvantum

flatpak install org.kde.WaylandDecoration.QGnomePlatform-decoration org.kde.PlatformTheme.QGnomePlatform
flatpak install flathub org.kde.KStyle.Kvantum

flatpak install flathub com.github.tchx84.Flatseal com.discordapp.Discord io.github.seadve.Mousai de.haeckerfelix.Shortwave \
    com.github.maoschanz.drawing org.gtk.Gtk3theme.Adwaita-dark org.gaphor.Gaphor com.github.iwalton3.jellyfin-media-player \
    com.github.muriloventuroso.givemelyrics org.jitsi.jitsi-meet app.drey.Dialect org.gtk.Gtk3theme.Breeze com.jgraph.drawio.desktop rest.insomnia.Insomnia \
    io.github.prateekmedia.appimagepool org.ferdium.Ferdium com.usebottles.bottles com.obsproject.Studio.Plugin.OBSVkCapture com.valvesoftware.Steam.Utility.gamescope \
    org.freedesktop.Platform.VulkanLayer.vkBasalt org.freedesktop.Platform.VulkanLayer.MangoHud org.freedesktop.Platform.VulkanLayer.gamescope \
    io.github.flattool.Warehouse io.github.fabrialberio.pinapp
