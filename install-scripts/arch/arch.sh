#!/bin/bash
# install rua before executing this
if [ "$EUID" -eq 0 ]
    then echo "Please run as a none root user"
    exit
fi
#source ./home/.profile
sudo pacman -Sy playerctl pavucontrol \
    alacritty firefox chromium htop qt5ct qt6ct feh imv qbittorrent proxychains picom scrot slop xclip moreutils gammastep inkscape \
    trash-cli mlocate xbindkeys alsa-utils xautolock dunst libnotify file-roller udisks2 udiskie xdg-user-dirs zsh zsh-completions \
    fzf neovim tree ripgrep fd the_silver_searcher luarocks power-profiles-daemon wget vulkan-headers gnumeric perl-image-exiftool \
    ueberzug ffmpegthumbnailer networkmanager-openvpn network-manager-applet openvpn kdeconnect sshfs curl lazygit mpv lxtask \
    obs-studio unrar networkmanager-openconnect polkit lxqt-policykit ufw wireguard-tools openresolv tcpdump eza bat tokei fd docx2txt w3m \
    rustup cmus fish powerline-fonts iptraf-ng sshfs mutt mupdf ncdu iftop curlftpfs rsync volumeicon bleachbit unzip btop transmission-cli \
    lynx odt2txt poppler imagemagick mediainfo perl-file-mimeinfo zathura zathura-pdf-mupdf zathura-ps zathura-djvu zathura-cb bemenu bemenu-x11 xorg-xinit bemenu-ncurses \
    libvncserver freerdp remmina pass pass-otp handbrake openssh dash shellcheck tcc tor tmux lxappearance cdrtools xdotool gnome-themes-standard adwaita-icon-theme \
    piper solaar reflector v4l2loopback-dkms fping bind atool starship zbar cairo pango nautilus jq baobab util-linux glow catdoc \
    xdg-desktop-portal xdg-desktop-portal-gtk xdg-desktop-portal-wlr xdg-desktop-portal \
    wlroots wayland-protocols bemenu-wayland swaybg swayidle swaylock slurp wl-clipboard chafa qt5-wayland qt6-wayland kvantum \
    glfw-wayland xdg-desktop-portal-wlr grim gamescope sway xorg-xwayland ecryptfs-utils polkit-gnome kdiskmark smartmontools acl wayvnc \
    yazi p7zip zoxide ouch nerd-fonts miller scrcpy gvfs-smb netcat mpd mpc ncmpcpp syncthing

paru -S z.lua lf-sixel-git librewolf-bin obfs4proxy \
    kora-icon-theme dragon-drop \
    wev wlrobs wlr-randr-git wdisplays wlopm # python-spotdl
chsh -s "$(which fish)"
#serveces: bluetooth.service ufw.service fstrim.timer
