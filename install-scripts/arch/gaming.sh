#!/bin/bash
if [ "$EUID" -eq 0 ]
    then echo "Please run as a none root user"
    exit
fi
sudo pacman -S vulkan-icd-loader lib32-vulkan-icd-loader vulkan-tools steam lutris gamemode lib32-gamemode mangohud
# dota2: MANGOHUD_CONFIG=cpu_temp,gpu_temp,ram,vram,media_player,gamemode,time,position=top-right gamemoderun mangohud %COMMAND% -vulkan -novid
