#!/bin/bash
if [ "$EUID" -eq 0 ]
    then echo "Please run as a none root user"
    exit
fi
sudo pacman -S virt-manager qemu ovmf vde2 ebtables dnsmasq bridge-utils openbsd-netcat virt-viewer
sudo usermod -a -G libvirt mhn
# services: libvirtd.service
