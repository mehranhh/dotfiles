#!/bin/bash
if [ "$EUID" -eq 0 ]
  then echo "Please run as a none root user"
  exit
fi
sudo pacman -S rustup nodejs npm python-pip go go-tools ccls yarn neovim vim ninja cmake lua-language-server \
    lldb delve yarn docker docker-compose cppcheck mitmproxy sshfs deno neovide ansible-core yamllint stylua mariadb postgresql k6 grpcurl
paru -S prisma-language-server
usermod -a -G docker mhn
rustup component add rust-analyzer
rustup component add clippy
rustup component add rustfmt
ln -sf "$(rustup which --toolchain stable rust-analyzer)" ~/.cargo/bin

yarn global add typescript typescript-language-server pyright vscode-langservers-extracted dockerfile-language-server-nodejs \
    @tailwindcss/language-server vim-language-server yaml-language-server eslint eslint_d prettier sql-language-server \
    bash-language-server
go install golang.org/x/tools/cmd/godoc@latest
go install golang.org/x/tools/cmd/vet@latest
go install golang.org/x/lint/golint@latest
go install golang.org/x/tools/gopls@latest
go install github.com/bufbuild/buf-language-server/cmd/bufls@latest
pip install cmake-language-server black
