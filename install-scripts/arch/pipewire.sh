#!/bin/bash
if [ "$EUID" -eq 0 ]
    then echo "Please run as a none root user"
    exit
fi
sudo pacman -S pipewire lib32-pipewire pipewire-docs pipewire-pulse pipewire-alsa pipewire-jack pipewire-audio wireplumber
