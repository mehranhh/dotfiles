#!/bin/bash
if [ "$EUID" -ne 0 ]
    then echo "Please run as root user"
    exit
fi
# wifi
# rfkill unblock wifi
# ip link set wlan0 up
# connmanctl
# connmanctl -> scan wifi
# connmanctl -> services 
# connmanctl -> agent on 
# connmanctl -> connect wifi_*****_psk
# connmanctl -> quit
loadkeys colemak
timedatectl set-ntp true
mkfs.ext4 -L ROOT /dev/nvme0n1p2
# mkfs.ext4 -L FILES /dev/nvme0n1p3
# e2label /dev/nvme0n1p3 FILES # set label
mkfs.fat -F 32 /dev/nvme0n1p1
fatlabel /dev/nvme0n1p1 BOOT
mount /dev/disk/by-label/ROOT /mnt
mkdir /mnt/boot
mkdir /mnt/home
mkdir /mnt/.files
mkdir /mnt/.remotes
mount /dev/disk/by-label/BOOT /mnt/boot
mount /dev/disk/by-label/FILES /mnt/.files
pacstrap /mnt base base-devel linux linux-firmware intel-ucode mkinitcpio
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt
