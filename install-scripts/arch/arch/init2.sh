#!/bin/bash
if [ "$EUID" -ne 0 ]
    then echo "Please run as root user"
    exit
fi
pacman -S vim nano git grub efibootmgr networkmanager network-manager-applet wireless_tools\
    linux-headers ntfs-3g bluez bluez-utils xdg-utils xdg-user-dirs openssh rsync
ln -sf /usr/share/zoneinfo/Asia/Tehran /etc/localtime
hwclock --systohc
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "export LANG=\"en_US.UTF-8\"" > /etc/locale.conf
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
grub-mkconfig -o /boot/grub/grub.cfg

groupadd files
mkdir /.files
setfacl -Rdm g:files:rwx /.files
setfacl -Rm g:files:rwx /.files

passwd
useradd -m mhn
usermod -a -G wheel,input,video,files mhn
passwd mhn
echo "M3HRAN" > /etc/hostname
echo "127.0.0.1        localhost" >> /etc/hosts
echo "::1              localhost" >> /etc/hosts
echo "127.0.1.1        M3HRAN.localdomain  M3HRAN" >> /etc/hosts
# echo "HandleLidSwitch=ignore" >> /etc/systemd/logind.conf
# echo "HandleLidSwitchExternalPower=ignore" >> /etc/systemd/logind.conf
# echo "AllowHibernation=no" >> /etc/systemd/logind.conf
# echo "AllowSuspendThenHibernate=no" >> /etc/systemd/logind.conf
# echo "AllowHybridSleep=no" >> /etc/systemd/logind.conf
# echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers # or visudo
# reboot
