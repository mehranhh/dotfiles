#!/bin/bash
if [ "$EUID" -ne 0 ]
    then echo "Please run as root user"
    exit
fi
systemctl enable --now NetworkManager
systemctl enable --now bluetooth.service
nmtui
pacman -Syu mesa lib32-mesa vulkan-intel vulkan-headers lib32-vulkan-intel # intel
pacman -S nvidia nvidia-utils nvidia-prime nvidia-dkms nvidia-settings lib32-nvidia-utils nvtop #nvidia
pacman -S mesa lib32-mesa xf86-video-amdgpu vulkan-radeon lib32-vulkan-radeon libva-mesa-driver lib32-libva-mesa-drive mesa-vdpau lib32-mesa-vdpau radeontop
pacman -S xorg
