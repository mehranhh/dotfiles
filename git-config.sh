#!/bin/bash
## git config
# git config --global user.name <name>
# git config --global user.email <email>
# git config --global user.signingkey <key_id>
# git config --global commit.gpgsign true
# git config --global init.defaultBranch main

## gpg key
# gpg --full-gen-key
# gpg --list-secret-keys
# gpg --armor --export <key_id>

## ssh key
# ssh-keygen -t ed25519 -C "<comment>"
