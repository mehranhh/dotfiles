#!/usr/bin/sh
rsync -avz ./vim/vimrc "$1:.vimrc"
rsync -avz ./tmux/tmux-server.conf "$1:~/.tmux.conf"
rsync --mkpath -avz ./fish-server/ "$1:~/.config/fish/"
