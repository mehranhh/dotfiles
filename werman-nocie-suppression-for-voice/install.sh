#!/bin/bash
paru -S noise-suppression-for-voice-git
mkdir -p ~/.config/noice-suppression-for-voice/
mkdir -p ~/.config/systemd/user/
ln -sf "$MLCFGDIR/werman-nocie-suppression-for-voice/filter-chain.conf" "$XDG_CONFIG_HOME/noice-suppression-for-voice/"
cp "$MLCFGDIR/werman-nocie-suppression-for-voice/pipewire-input-filter-chain.service" ~/.config/systemd/user/
systemctl enable --user --now pipewire-input-filter-chain.service
